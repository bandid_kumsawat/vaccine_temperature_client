// option
function packstr(val) {
  var str = "X : "+val.x+" , "+"Y : "+val.y;
  return str;
}

function convert_time(val) {
  var convert_utc = val;
  var utc_plus=0;
  convert_utc = convert_utc.toString();
  convert_utc = convert_utc.substring(0, 10);
  utc_plus = parseInt(convert_utc);
  return utc_plus;
}

function time_set (val) {
  var convert_time = val;
  var success,timestamp;
  convert_time = convert_time.split(" ");
  success = convert_time[0]+"T"+convert_time[1]+"+07:00";
  timestamp = moment(success).format('x');
  return timestamp;
}

function real_date(){
  setInterval(function(){
    $('#real_date').text(moment().format('DD/MM/YYYY  HH:mm:ss'));
  }, 1000);
}
